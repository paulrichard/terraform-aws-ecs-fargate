# README.md

## Introduction

This repository contains the Terraform configuration for the project_one and project_two projects. The configuration is split into two different sections: `project_one` and `project_two`. 

Each project has a `.gitlab-ci.yml` file in its respective folder that includes the pipeline stages for validating, building, deploying, and cleaning up the Terraform infrastructure.

## GitLab CI/CD pipeline

The pipeline is triggered by changes to any `.tf` files in the `project_one` or `project_two` folders. 

The pipeline stages are:
- triggers

## project_one

The project_one project includes the following Terraform modules:
- kms: creates an AWS Key Management Service (KMS) key
- rds: creates an Amazon Relational Database Service (RDS) instance
- ecs: creates an Amazon Elastic Container Service (ECS) cluster
- alb: creates an Amazon Application Load Balancer (ALB)
- api: creates an ECS task definition and service for the API
- gateway: creates an ECS task definition and service for the Gateway

Please note that for more detailed information about the project_one project, please check the README inside the project_one folder.

## project_two

The project_two project includes the following Terraform modules:
- kms: creates an AWS Key Management Service (KMS) key
- rds: creates an Amazon Relational Database Service (RDS) instance
- ecs: creates an Amazon Elastic Container Service (ECS) cluster
- alb: creates an Amazon Application Load Balancer (ALB)
- api: creates an ECS task definition and service for the API
- consumer: creates an ECS task definition and service for the CONSUMER
- read: creates an ECS task definition and service for the READ

## Terraform state

Each project has its own Terraform state stored within the GitLab environment. Currently, the pipeline is using the development environment, but it can easily be configured to use other environments as well. The naming convention for the Terraform state is 'project'-'environment', for example: "project_one-development".

## Note

Please note that the above code is just an example, it may require adjustments to match your specific GitLab CI/CD setup and your specific needs for the project_one and project_two projects.

In the `project_one` and `project_two` folders, you will find the `.gitlab-ci.yml` file where you can see the pipeline stages such as validate, build, deploy and cleanup. These stages uses modules like Base, Terraform and Jobs.

The modules `api` and `gateway` are creating the ECS task definition and service for the API and the Gateway respectively.

The module `alb` is creating an Amazon Application Load Balancer, this is used to distribute the incoming traffic to the correct target group.

The `kms`, `rds`, and `ecs` are creating, respectively, an AWS Key Management Service key, an Amazon Relational Database Service instance, and an Amazon Elastic Container Service cluster.
