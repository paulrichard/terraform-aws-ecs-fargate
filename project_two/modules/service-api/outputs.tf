output "security_group_id" {
  description = "The ID of the security group of the ECS Service"
  value       = aws_security_group.this.id
}