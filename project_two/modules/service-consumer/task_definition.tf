resource "aws_ecs_task_definition" "this" {
  family                   = local.name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.exec.arn

  container_definitions = jsonencode([
    {
      name      = "${local.name}"
      image     = "${module.ecr.repository_url}:latest"
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      secrets = [
        {
          "name" : "db.username",
          "valueFrom" : "${var.rds_secret_arn}:username::"
        },
        {
          "name" : "db.password",
          "valueFrom" : "${var.rds_secret_arn}:password::"
        },
        {
          "name" : "my.secret",
          "valueFrom" : "${aws_ssm_parameter.my_secret.arn}"
        }
      ]
      environment = [
        {
          "name" : "PORT",
          "value" : "80",
        },
        {
          "name" : "db.port",
          "value" : "3306"
        },
        {
          "name" : "db.schema",
          "value" : "${var.tags.Project}"
        },
        {
          "name" : "db.host",
          "value" : "${var.rds_address}",
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-group"         = "${local.name}"
          "awslogs-region"        = "${data.aws_region.current.name}"
          "awslogs-stream-prefix" = "${var.tags.Project}"
        }
      }
    }
  ])

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}