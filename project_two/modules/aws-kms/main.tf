resource "aws_kms_key" "this" {
  description             = "KMS Key for encrypting all sensitive data in the application"
  key_usage               = "ENCRYPT_DECRYPT"
  enable_key_rotation     = true
  deletion_window_in_days = 30

  tags = var.tags
}

resource "aws_kms_alias" "this" {
  name          = "alias/eai/apps/${var.tags.Project}"
  target_key_id = aws_kms_key.this.key_id
}