locals {
  region = "us-east-1"
  name   = "projecttwo" #Very important, change this will re-create resources

  tags = {
    Name    = local.name
    Project = local.name
    Owner   = "Paul Richard"
  }
}

module "kms" {
  source = "./modules/aws-kms"

  tags   = local.tags
  vpc_id = var.vpc_id
}

module "rds" {
  source = "./modules/aws-rds"

  name        = local.name
  kms_key_arn = module.kms.key_arn
  tags        = local.tags
  vpc_id      = var.vpc_id
}

module "ecs" {
  source = "./modules/aws-ecs-cluster"

  tags   = local.tags
  vpc_id = var.vpc_id
}

module "api" {
  source = "./modules/service-api"

  name           = "api" #Will be 'project_name-service_name #projecttwo-api
  cluster_id     = module.ecs.cluster_id
  dns_private    = var.dns_private
  kms_key_arn    = module.kms.key_arn
  rds_address    = module.rds.address
  rds_secret_arn = module.rds.secret_arn
  tags           = local.tags
  vpc_id         = var.vpc_id
}

module "consumer" {
  source = "./modules/service-consumer"

  name           = "consumer" #Will be 'project_name-service_name #projecttwo-consumer
  cluster_id     = module.ecs.cluster_id
  dns_private    = var.dns_private
  kms_key_arn    = module.kms.key_arn
  rds_address    = module.rds.address
  rds_secret_arn = module.rds.secret_arn
  tags           = local.tags
  vpc_id         = var.vpc_id
}

################################################################################
# Security Group rules
################################################################################

resource "aws_security_group_rule" "rds_api" {
  description              = "Allow traffic from the api service"
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = module.api.security_group_id
  security_group_id        = module.rds.security_group_id
}

resource "aws_security_group_rule" "rds_consumer" {
  description              = "Allow traffic from the consumer service"
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = module.consumer.security_group_id
  security_group_id        = module.rds.security_group_id
}

resource "aws_security_group_rule" "api" {
  description       = "Allow traffic from anywhere"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = module.api.security_group_id
}
