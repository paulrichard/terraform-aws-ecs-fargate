## Overview
This Terraform code creates resources for a project named "project_two". It includes the following resources: 
- An AWS KMS key
- An ECS cluster
- An RDS instance
- An API service 
- Security group rules for the RDS and API resources

## Requirements
- Terraform 0.14+
- The `vpc_id` variable needs to be set as a CI/CD variable in GitLab
- AWS credentials are set in the GitLab project

## How to use
1. Set the `vpc_id` variable as a CI/CD variable in GitLab
2. The Terraform code is executed through a GitLab CI/CD pipeline, no further action is required.

## Note
- The code includes a commented out module for creating a gateway service. This service is not currently available and should not be uncommented in the main.tf file.
- Only the development environment is currently available.
