variable "vpc_id" {
  description = "The ID of the VPC for the ECS cluster"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "environment" {
  description = "The environment for which the infrastructure is being created (e.g. 'production', 'staging', 'development')"
  default     = "development"
}

variable "dns_private" {
  description = "The Private Domain to create the DNS records using AWS CloudMap"
  type        = string
  default     = ""
}

variable "dns_public" {
  description = "The Public Domain to create the DNS records"
  type        = string
  default     = ""
}