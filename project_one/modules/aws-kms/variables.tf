variable "name" {
  description = "What to name the instance and all of its associated resources"
  type        = string
  default     = ""
}

variable "vpc_id" {
  description = "The ID of the VPC for the ECS cluster"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}