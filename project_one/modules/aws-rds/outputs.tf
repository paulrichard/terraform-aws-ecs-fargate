output "security_group_id" {
  description = "The ID of the security group of the RDS"
  value       = aws_security_group.this.id
}

output "secret_arn" {
  description = "The ARN of the RDS secret"
  value       = aws_secretsmanager_secret.this.arn
}

output "address" {
  description = "The endpoint (without the port) of the RDS"
  value       = aws_db_instance.mysql.address
}