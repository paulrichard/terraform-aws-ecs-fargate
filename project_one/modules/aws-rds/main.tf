data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Tier = "Private"
  }
}

resource "random_string" "this" {
  length  = 3
  special = false
  numeric = false
  upper   = false
}

resource "random_password" "this" {
  length           = 40
  special          = true
  min_special      = 5
  override_special = "!#$%^&*()-_=+[]{}<>:?"
}

resource "aws_secretsmanager_secret" "this" {
  name                    = "eai/apps/rds-${var.name}"
  kms_key_id              = var.kms_key_arn
  recovery_window_in_days = 0

  tags = var.tags
}

resource "aws_secretsmanager_secret_version" "this" {
  secret_id = aws_secretsmanager_secret.this.id
  secret_string = jsonencode(
    {
      username = var.name
      password = random_password.this.result
    }
  )
}

################################################################################
# Create the RDS Database
################################################################################

resource "aws_db_instance" "mysql" {
  allocated_storage      = 5
  db_name                = var.name
  identifier             = "${var.name}-${random_string.this.id}"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t4g.micro"
  kms_key_id             = var.kms_key_arn
  storage_encrypted      = true
  username               = var.name
  password               = sensitive(random_password.this.result)
  parameter_group_name   = "default.mysql8.0"
  vpc_security_group_ids = [aws_security_group.this.id]
  db_subnet_group_name   = aws_db_subnet_group.this.name
  port                   = 3306
  skip_final_snapshot    = true
  deletion_protection    = false
  apply_immediately      = true

  tags = var.tags
}

resource "aws_db_subnet_group" "this" {
  name       = var.name
  subnet_ids = toset(flatten([data.aws_subnets.private.ids]))

  tags = var.tags
}

################################################################################
# Create the Security Group for the RDS
################################################################################

resource "aws_security_group" "this" {
  name   = "rds-${var.name}"
  vpc_id = var.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    # Necessary if changing 'name' or 'name_prefix' properties.
    create_before_destroy = true
  }

  tags = var.tags
}