resource "aws_ecs_task_definition" "this" {
  family                   = local.name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.exec.arn

  container_definitions = jsonencode([
    {
      name      = "${local.name}"
      image     = "${module.ecr.repository_url}:latest"
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      secrets = [
        {
          "name" : "my.secret",
          "valueFrom" : "${aws_ssm_parameter.my_secret.arn}"
        }
      ]
      environment = [
        {
          "name" : "PORT",
          "value" : "80",
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-group"         = "${local.name}"
          "awslogs-region"        = "${data.aws_region.current.name}"
          "awslogs-stream-prefix" = "${var.tags.Project}"
        }
      }
    }
  ])

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}