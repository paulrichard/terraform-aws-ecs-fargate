output "security_group_id" {
  description = "The ID of the security group of the ECS Service"
  value       = aws_security_group.this.id
}

output "dns_public" {
  description = "FQDN built using the zone public domain and name"
  value       = aws_route53_record.this.fqdn
}

output "dns_private" {
  description = "FQDN built using the zone private domain and name"
  value       = aws_service_discovery_service.this.dns_config
}