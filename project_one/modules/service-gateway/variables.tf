variable "name" {
  description = "What to name the service and all of its associated resources"
  type        = string
  default     = ""
}

variable "vpc_id" {
  description = "The ID of the VPC for the ECS cluster"
  type        = string
  default     = ""
}

variable "cluster_id" {
  description = "The ID of the cluster for the ECS service"
  type        = string
  default     = ""
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "kms_key_arn" {
  description = "The ARN of the KMS key used to encrypt data"
  type        = string
  default     = ""
}

variable "alb_arn" {
  description = "The ARN of the load balancer (matches id)"
  type        = string
  default     = ""
}

variable "alb_dns" {
  description = "The DNS name of the load balancer"
  type        = string
  default     = ""
}

variable "alb_zone_id" {
  description = "The zone_id of the load balancer to assist with creating DNS records"
  type        = string
  default     = ""
}

variable "dns_private" {
  description = "The Private Domain to create the DNS records using AWS CloudMap"
  type        = string
  default     = ""
}

variable "dns_public" {
  description = "The Public Domain to create the DNS records"
  type        = string
  default     = ""
}
