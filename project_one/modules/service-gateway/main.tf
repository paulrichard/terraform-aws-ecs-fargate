data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Tier = "Private"
  }
}

data "aws_service_discovery_dns_namespace" "this" {
  name = var.dns_private
  type = "DNS_PRIVATE"
}

data "aws_route53_zone" "public" {
  name         = var.dns_public
  private_zone = false
}

data "aws_region" "current" {}
data "aws_partition" "current" {}
data "aws_caller_identity" "current" {}

locals {
  name     = "${var.tags.Project}-${var.name}"
  ssm_path = "/my_company/apps/${var.tags.Project}/${var.name}"
}

################################################################################
# Create the ECS Service
################################################################################

resource "aws_ecs_service" "this" {
  name            = local.name
  cluster         = var.cluster_id
  task_definition = aws_ecs_task_definition.this.arn
  desired_count   = 1

  launch_type = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.this.id}"]
    subnets          = toset(flatten([data.aws_subnets.private.ids]))
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.this.arn
    container_name   = local.name
    container_port   = 80
  }

  service_connect_configuration {
    enabled   = true
    namespace = data.aws_service_discovery_dns_namespace.this.arn

    log_configuration {
      log_driver = "awslogs"
      options = {
        awslogs-region        = data.aws_region.current.name
        awslogs-group         = local.name
        awslogs-stream-prefix = var.tags.Project
      }
    }
  }

  service_registries {
    registry_arn = aws_service_discovery_service.this.arn
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

################################################################################
# Create the ECR repository for the ECS Service
################################################################################

module "ecr" {
  source  = "terraform-aws-modules/ecr/aws"
  version = "1.5.1"

  repository_name = local.name

  repository_encryption_type = "KMS"
  repository_kms_key         = var.kms_key_arn
  repository_force_delete    = true

  repository_read_write_access_arns = ["arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:root"]

  create_lifecycle_policy = true
  repository_lifecycle_policy = jsonencode({
    rules = [
      {
        rulePriority = 1,
        description  = "Keep last 30 images",
        selection = {
          tagStatus     = "tagged",
          tagPrefixList = ["v"],
          countType     = "imageCountMoreThan",
          countNumber   = 30
        },
        action = {
          type = "expire"
        }
      }
    ]
  })

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

################################################################################
# Create Target Group and ALB listener
################################################################################

resource "aws_lb_target_group" "this" {
  name        = local.name
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = var.alb_arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.acm.acm_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

resource "aws_lb_listener_rule" "this" {
  listener_arn = aws_lb_listener.this.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }

  condition {
    host_header {
      values = ["${local.name}.${data.aws_route53_zone.public.name}"]
    }
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

################################################################################
# Create ACM, DNS records, CloudMap
################################################################################

resource "aws_service_discovery_service" "this" {
  name = local.name

  dns_config {
    namespace_id = data.aws_service_discovery_dns_namespace.this.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

resource "aws_route53_record" "this" {
  zone_id = data.aws_route53_zone.public.zone_id
  name    = "${local.name}.${data.aws_route53_zone.public.name}"
  type    = "A"

  alias {
    name                   = var.alb_dns
    zone_id                = var.alb_zone_id
    evaluate_target_health = true
  }
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 4.0"

  domain_name = "${local.name}.${data.aws_route53_zone.public.name}"

  create_route53_records  = false
  validation_record_fqdns = module.route53_records.validation_route53_record_fqdns
}

module "route53_records" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 4.0"

  create_certificate          = false
  create_route53_records_only = true

  distinct_domain_names = module.acm.distinct_domain_names
  zone_id               = data.aws_route53_zone.public.zone_id

  acm_certificate_domain_validation_options = module.acm.acm_certificate_domain_validation_options
}

################################################################################
# Create CloudWatch for the ECS Service
################################################################################

resource "aws_cloudwatch_log_group" "this" {
  name              = local.name
  retention_in_days = 7

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

################################################################################
# Create SSM parameters to use with the Task Definition
################################################################################

resource "aws_ssm_parameter" "my_secret" {
  name   = "${local.ssm_path}/my_secret"
  type   = "SecureString"
  key_id = var.kms_key_arn
  value  = "sensitive_value"

  lifecycle {
    ignore_changes = [value]
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

################################################################################
# Create the IAM Role for the ECS Service
################################################################################

resource "aws_iam_role" "exec" {
  name = "${local.name}-exec"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name   = "secrets"
    policy = data.aws_iam_policy_document.exec_policy.json
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}

data "aws_iam_policy_document" "exec_policy" {
  statement {
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "*"
    ]
  }

  statement {
    actions = [
      "kms:Decrypt",
      "kms:Encrypt",
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:ListSecretVersionIds",
      "secretsmanager:GetResourcePolicy",
      "ssm:GetParameters",
    ]
    resources = [
      aws_ssm_parameter.my_secret.arn,
      var.kms_key_arn,
    ]
  }
  statement {
    actions = [
      "secretsmanager:ListSecrets"
    ]
    resources = [
      "*"
    ]
  }
}

################################################################################
# Create the Security Group for the ECS Service
################################################################################

resource "aws_security_group" "this" {
  name   = "ecs-${local.name}"
  vpc_id = var.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    # Necessary if changing 'name' or 'name_prefix' properties.
    create_before_destroy = true
  }

  tags = merge(
    var.tags,
    {
      Name = local.name
    },
  )
}