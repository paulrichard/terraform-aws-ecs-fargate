output "security_group_id" {
  description = "The ID of the security group of the RDS"
  value       = aws_security_group.this.id
}

output "id" {
  description = "The ID and ARN of the load balancer we created"
  value       = aws_lb.this.id
}

output "arn" {
  description = "The ID and ARN of the load balancer we created"
  value       = aws_lb.this.arn
}

output "dns_name" {
  description = "The DNS name of the load balancer"
  value       = aws_lb.this.dns_name
}

output "zone_id" {
  description = "The zone_id of the load balancer to assist with creating DNS records"
  value       = aws_lb.this.zone_id
}