data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Tier = "Public"
  }
}

resource "random_string" "this" {
  length  = 5
  special = false
  numeric = false
  upper   = false
}

resource "aws_lb" "this" {
  name               = var.tags.Project
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.this.id]
  subnets            = toset(flatten([data.aws_subnets.public.ids]))

  enable_deletion_protection = false

  access_logs {
    bucket  = module.s3.s3_bucket_id
    prefix  = "alb-${var.tags.Project}"
    enabled = true
  }

  tags = var.tags
}

################################################################################
# Create the S3 bucket for the ALB logs
################################################################################

module "s3" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.6.0"

  bucket = "alb-${var.tags.Project}-${random_string.this.id}"
  acl    = "log-delivery-write"

  # Allow deletion of non-empty bucket
  force_destroy = true

  attach_elb_log_delivery_policy = true # Required for ALB logs
  attach_lb_log_delivery_policy  = true # Required for ALB/NLB logs

  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = var.kms_key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

################################################################################
# Create the Security Group for the ECS Service
################################################################################

resource "aws_security_group" "this" {
  name   = "alb-${var.tags.Project}"
  vpc_id = var.vpc_id

  ingress {
    description      = "Allow HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    # Necessary if changing 'name' or 'name_prefix' properties.
    create_before_destroy = true
  }

  tags = var.tags
}