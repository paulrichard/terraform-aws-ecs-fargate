output "dns_public" {
  description = "FQDN built using the zone public domain and name"
  value       = module.gateway.dns_public
}