# README

This is the Terraform code for the "project_one" project. It creates an Amazon Elastic Container Service (ECS) service and an Elastic Container Registry (ECR) repository for the service. It also creates a target group and an Application Load Balancer (ALB) listener for the service.

## Variables

- `vpc_id`: The ID of the Virtual Private Cloud (VPC) in which the resources will be created
- `name`: The name of the ECS service and the ECR repository
- `cluster_id`: The ID of the ECS cluster in which the service will be created
- `kms_key_arn`: The Amazon Resource Name (ARN) of the Key Management Service (KMS) key to use for encryption
- `alb_arn`: The ARN of the Application Load Balancer to use
- `tags`: A map of tags to apply to all resources
- `domain_name` : the domain name defined in the gitlab ci cd TFVARS file

## Resources

- `aws_ecs_service`: The ECS service
- `aws_ecr_repository`: The ECR repository
- `aws_lb_target_group`: The target group for the service
- `aws_lb_listener`: The listener for the Application Load Balancer
- `aws_route53_record`: The dns record for the service

## Modules

- `ecr`: A module for creating the ECR repository
- `acm`: A module for creating a certificate for the ALB listener
- `alb`: A module for creating the Application Load Balancer and listener

## API Module

It creates an Amazon Elastic Container Service (ECS) service and an Elastic Container Registry (ECR) repository for the service. It also creates a service discovery using CloudMap, and creates DNS records for the service.

### Variables

- `vpc_id`: The ID of the Virtual Private Cloud (VPC) in which the resources will be created
- `name`: The name of the ECS service and the ECR repository
- `cluster_id`: The ID of the ECS cluster in which the service will be created
- `kms_key_arn`: The Amazon Resource Name (ARN) of the Key Management Service (KMS) key to use for encryption
- `tags`: A map of tags to apply to all resources

### Resources

- `aws_ecs_service`: The ECS service
- `aws_ecr_repository`: The ECR repository
- `aws_service_discovery_service`: The service discovery service

### Data Sources

- `aws_subnets`: The private subnets in the VPC
- `aws_service_discovery_dns_namespace`: The local DNS namespace
- `aws_region`: The current region
- `aws_partition`: The current partition
- `aws_caller_identity`: The current caller identity

### Modules

- `ecr`: A module for creating the ECR repository

## Gateway Module

In the gateway module, it uses the domain_name defined in the gitlab ci cd TFVARS file to create the dns records, and also, adds a listener into the alb module from the root of the project, which also add the acm certificate and forward the traffic to the gateway target group.
