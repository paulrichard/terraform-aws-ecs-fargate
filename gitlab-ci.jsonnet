local variables = {
  TF_ROOT: '${CI_PROJECT_DIR}/${TF_PROJECT_NAME}',
  TF_ADDRESS: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_PROJECT_NAME}',
};

local stages = [  'validate',  'build',  'deploy',];

local image = 'registry.gitlab.com/gitlab-org/terraform-images/stable:latest';

local cache = {
  key: '"${TF_ROOT}"',
  paths: [
    '${TF_ROOT}/.terraform/',
  ],
};

local before_script = [  'cd ${TF_ROOT}',];

local validate = {
  stage: 'validate',
  script: [
    'gitlab-terraform fmt',
    'gitlab-terraform validate',
  ],
};

local plan = {
  stage: 'build',
  variables: {
    TF_ADDRESS: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_PROJECT_NAME}-development',
    TF_STATE_NAME: '${TF_PROJECT_NAME}-development',
  },
  environment: {
    name: '${TF_PROJECT_NAME}',
    deployment_tier: 'development',
  },
  script: [
    'gitlab-terraform plan -var vpc_id=${AWS_VPC_ID}',
    'gitlab-terraform plan-json -var vpc_id=${AWS_VPC_ID}',
  ],
  artifacts: {
    name: 'plan',
    paths: [
      '${TF_ROOT}/plan.cache',
    ],
    reports: {
      terraform: '${TF_ROOT}/plan.json',
    },
  },
  needs: [
    'validate',
  ],
};

local apply = {
stage: 'deploy',
  variables: {
    TF_ADDRESS: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${TF_PROJECT_NAME}-development',
    TF_STATE_NAME: '${TF_PROJECT_NAME}-development',
  },
  environment: {
    name: '${TF_PROJECT_NAME}',
    deployment_tier: 'development',
  },
  script: [
    'gitlab-terraform apply -var vpc_id=${AWS_VPC_ID}',
  ],
  dependencies: [ 'plan',],
  needs: [ 'plan',],
  when: 'manual',
};

{
  variables: variables,
  stages: stages,
  cache: cache,
  before_script: before_script,
  validate: validate,
  plan: plan,
  apply: apply,
}
